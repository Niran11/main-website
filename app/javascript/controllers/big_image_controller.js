import { Controller } from "@hotwired/stimulus";

// Connects to data-controller="big-image"
export default class extends Controller {
  show(ev) {
    const container = document.createElement("div");
    container.classList.add("big-image");
    container.dataset.controller = "big-image";
    container.dataset.action = "click->big-image#hide";
    const overlay = document.createElement("div");
    overlay.classList.add("overlay");
    const contentContainer = document.createElement("div");
    contentContainer.classList.add("container");
    const img = document.createElement("img");
    img.src = ev.target.src;
    img.classList.add("image");
    contentContainer.append(img);
    container.append(overlay, contentContainer);
    document.body.append(container);
  }
  hide() {
    this.element.remove();
  }
}
