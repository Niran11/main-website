import { Controller } from "@hotwired/stimulus";

// Connects to data-controller="theme"
export default class extends Controller {
  initialize() {
    this.setTheme();
  }
  toggle() {
    if (localStorage.theme === "dark") {
      localStorage.theme = "light";
    } else {
      localStorage.theme = "dark";
    }
    this.setTheme();
  }
  setTheme() {
    const lightTheme =
      localStorage.theme === "light" ||
      (!("theme" in localStorage) &&
        window.matchMedia("(prefers-color-scheme: light)").matches);
    const classList = document.documentElement.classList;
    if (lightTheme) {
      classList.remove("dark");
    } else {
      classList.add("dark");
    }
  }
}
