import { Controller } from "@hotwired/stimulus";

// Connects to data-controller="project-close"
export default class extends Controller {
  static targets = ["closeButton"];
  close(ev) {
    if (ev.target.classList.contains("container")) {
      this.closeButtonTarget.querySelector("a").click();
    }
  }
}
