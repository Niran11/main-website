import { Controller } from "@hotwired/stimulus";

// Connects to data-controller="cleaner"
export default class extends Controller {
  clean(ev) {
    ev.preventDefault();
    this.element.innerHTML = "";
  }
}
