require "set"

class ProjectsController < ApplicationController
  def index
    @projects = Project.preload(:tags).sorted
    @active_tags = Set.new
    @tags = Tag.sorted
  end

  def show
    @project = Project.preload(:project_urls, :tags, :project_media).find(params[:id])
    respond_to do |format|
      format.html
      format.turbo_stream
    end
  end

  def search
    filters = search_params
    idx = filters[:active_ids].find_index(filters[:id])
    if idx.nil?
      filters[:active_ids] << filters[:id]
    else
      filters[:active_ids].delete_at(idx)
    end
    active_ids = filters[:active_ids].to_set
    @projects = Project
      .preload(:tags)
      .joins(:tags)
      .distinct
      .sorted
    if !active_ids.empty?
      @projects = @projects.where(tags: filters[:active_ids])
    end
    @projects = @projects.filter { |p| project_has_tags?(active_ids, p) }
    @tags = Tag.sorted
    @active_tags = @tags.select { |t| active_ids.include?(t.id.to_s) }.to_set { |t| t.id }
    respond_to do |format|
      format.html { render :index }
      format.turbo_stream
    end
  end

  private

  def project_has_tags?(active_ids, p)
    active_ids.empty?() || active_ids.subset?(p.tags.to_set { |t| t.id.to_s })
  end

  def search_params
    params.require(:tags).permit(:id, active_ids: []).with_defaults(active_ids: [])
  end
end
