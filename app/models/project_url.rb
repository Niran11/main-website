class ProjectUrl < ApplicationRecord
  belongs_to :project
  validates :label, presence: true
  validates :url, presence: true, uniqueness: { scope: :project_id }
end
