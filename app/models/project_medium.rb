class ProjectMedium < ApplicationRecord
  belongs_to :project
  validates :filename, presence: true, uniqueness: { scope: :project_id }
end
