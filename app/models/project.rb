require "set"

class Project < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :code, presence: true, uniqueness: true
  validates :summary, presence: true
  validates :description, presence: true
  validates :weight, presence: true

  has_many :project_tags, dependent: :destroy

  has_many :project_urls, dependent: :destroy
  has_many :project_media, dependent: :destroy
  has_many :tags, through: :project_tags

  scope :preload_all, -> { preload(:project_urls, :project_media, :tags) }
  scope :sorted, -> { order(weight: :desc, name: :asc) }

  def update_tags(tags)
    tags_upper = tags.map { |t| t.upcase }
    existing = Tag.where("UPPER(name) IN (?)", tags_upper)
    self.tags = existing
    tags
      .reject { |t| existing.find { |e| e.name.upcase == t.upcase } }
      .each { |t| self.tags.create(name: t) }
  end

  def update_urls(urls)
    urls_hash = urls.to_h { |u| [u[:url], u[:label]] }
    existing = ProjectUrl.where("url IN (?)", urls_hash.keys()).all
    existing_hash = existing.to_h { |u| [u.url, u] }
    self.project_urls.each do |u|
      if existing_hash.include? u.url
        if u.text != urls_hash[u.url]
          u.text = urls_hash[u.url]
          u.save!
        end
      else
        self.project_urls.delete(u)
      end
    end
    urls.each do |u|
      self.project_urls.create(u) unless existing_hash.include?(u[:url])
    end
  end

  def update_media(media)
    existing = ProjectMedium.where(filename: media, project_id: self.id).all
    existing_set = existing.to_set { |m| m.filename }
    self.project_media.each do |m|
      self.project_media.delete(m) unless existing_set.include? m.filename
    end
    media.each do |m|
      self.project_media.create(filename: m) unless existing_set.include? m
    end
  end
end
