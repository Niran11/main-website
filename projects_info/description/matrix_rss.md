Atom/RSS feed bot. It reads feeds he has in his database and sends new items in rss feeds.
This bot pretty simple - checks given feeds every x minutes, checks ids of posts and if some id is not in its database
- sends to channels that want news from that feed and add these posts to database.
Then every Y days it clears old posts to not waste disk space,
and on channel leave or feed deletion clears all ids saved for given feed if given feed is no longer used anywhere.

It doesn't support any fancy options like for example filtering posts to send, I don't have the need for it.
Only things you can change is feed title to send and whether to send post body or not for given feed.
Neither does it check if body of post contains something dangerous or something matrix would be unable to display.
