Website to view upcoming fencing tournaments in Poland in order from upcoming to most future ones.
It also has working filtering for age categories/weapons.
It works by scraping tournament data from official site once a day, storing it in sqlite db and send to clients when they load site.
\
\
It got deprecated/abandoned in favor of [fencfo](https://fencfo.tzwz.xyz/)
which is rewrite in elixir and has more functionalities.
