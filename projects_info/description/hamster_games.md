This project allows you play having eg. 2 computers and 3 people wanting to play.
Idea is that one person creates virtual room, sends url to it to other person.
Each person creates users present on each computer and select game to play together (eg. pong, bomberman).
Game has to be able to handle your users amount and distribution, which you can see in game's card.
Then you can end game and select different one in same room.

Games work in a way that first user on given computer uses eg. WASD for controls, second uses arrows,
then on second computer only person uses WASD.
Or whatever controls presets given game uses.

Uses websockets for low-latency game communication.
