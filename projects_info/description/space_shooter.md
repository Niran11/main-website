Generic space shooter game made for university class.
You control spaceship on the bottom that constantly shoots bullets.
You have to kill enemies and survive as long as you can.
There are different kinds enemies, such as ones that move to some predefined position, ones following some path constantly,
ones shooting bullets, bosses having hp and special types of bullets.
