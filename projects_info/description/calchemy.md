Collaborative, online calendar without login.
You can create new calendar, add/modify/delete events in it.
All you have is url to it which you can share with friends and family.
If they edit something in it - everyone gonna see it.

You can plan a trip with this, you can use it with your class to store exams dates,
you can use it with your friends to give them information when you will be unavailable for meetings.
Whatever you need.
