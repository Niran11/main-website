Fantasy MMORPG in the browser. Move around endless, randomly generated map,
fight monsters, upgrade your character's stats, gather equipment and fight bosses.
You can also descend to lower world's floors to meet stronger foes, but you have to find some stairs there.
\
\
Every entity is one elixir's process.
To change state of entity it has to receive message and then process and apply action to itself.
If some action is impossible for entity to do, then error is thrown and entity performing action gets notified
that it can't do some action to that target. Eg. attacking a wall ends up in failure.
\
\
Game map is split into "map part"s of size 50x50 tiles.
Whenever user gets near edge of his current map part then new spawner gets started.
After spawning entities on map part a "watcher" remains,
storing players staying on its map part or parts next to it.
Entity watchers are kept in `Registry` so there's no 2 watchers (or spawners) on one map part.
If there's no player next to map part for a minute then watcher kills all entities on that map part
and terminates. This way map part can be filled with enemies again.
