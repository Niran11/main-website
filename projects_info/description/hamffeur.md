Hamster + chauffeur = Hamffeur

Takes you around (hard drive).
\
\
Server/computer storage explorer and music player with playlists.
Available from your browser.

You can eg.:
- create files
- rename files
- remove files
- view files content (including images, videos, music, etc.)
- move files/directories around
- edit text files

\
In music view:
- add file to queue
- play playlist in order
- change played file
- save playlist in DB
- open saved playlist

\
In gallery view:
- view directories having images
- view images in given directory
- move around image files
