Political/economical/war web game in real time.

You want to rule country?
You have to win proper elections and be president for a month (unless congress decides otherwise).

You want to be in congress? You have to win elections.

You want to trade in monetary market, exchanging currencies for one another?
No problem, but it's social game - other, real players have to buy it.

You want to take part in a war?
Okay, get to correct country (and region) and do damage for your country to help it win a round.
The battle? You have to win 10 rounds, each lasts for real time hour(s) and only real people take part in them.

Feeling like businessman?
You can create company and hire players to work in them, and sell or use products.
Or start winning equipment on auctions and selling for more money.
And enjoy taxes.
\
\
In short - it's slow, real time, social game.
