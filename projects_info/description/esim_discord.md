There is (or used to be, depends when you are reading this) a browser game callled e-sim.
It was pretty slow game where you had to press two buttons daily (in later years 2-3 times a day)
and sometimes participate in battle for your country (as in real life country, at least by name).
There was a layer of politics, items trade, newspapers, etc. to all that too.

This bot uses game's API to get, calculate and show some stuff from the game.
It is able to, for example, approximate how much damage user will do,
show info about user (total damage, level, location, equipment, etc.),
calculate expected battle drops, weapons used and hits done,
notify people that some battle is near end and they should oversee it,
etc.
\
\
I don't play this game for years and only touch this thing if/when someone
from the game finds me and complains that the bot is not running anymore.
