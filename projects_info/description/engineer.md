A program that allows you build and deploy any application.
Something like more general version of
[cargo-watch](https://crates.io/crates/cargo-watch) from rust world.
It has separate steps for building (which can have multiple steps) and deploying application.
Deployments are restarted only if building did not fail.
Thanks to this, your deployment can have less downtime due to compiling and running application in separate threads.
Also when developing something that is executed on different machine, you can
change something on your machine and this program will
automatically redeploy your application wherever you need it running.
