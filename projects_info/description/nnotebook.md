A simple notebook for terminal.
I needed that as [taskwarrior](https://taskwarrior.org/) has many features I didn't need,
but didn't have option to write long text notes I needed.

All you need is terminal window and `$EDITOR` variable.
Editing note is just writing text in selected text editor.

I abandoned this project as eventually I needed more advanced/graphical solution.
