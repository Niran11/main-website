Classic boulder dash game made for university class.
In the game player walks around on randomly generated map where he can find falling down boulders,
spilling lava, points giving diamonds and escape that generates new level.
Player's goal is to pick as many diamonds as he can,
don't die in the process and go to next level until time goes out.
