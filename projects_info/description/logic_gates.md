Project for computer programming class.
It's purpose is to calculate circuit made of logic gates and calculate output signal's value.
It takes 2 input files.
First input files represents the circuit which consists of input and output signals (cables) and logic gates.
Second input file is status file.
It stores information about different input values to input signals
for which program should calculate value of output signal.
See `testCircuit` and `testStatus` for how they look.
In the output it returns input signals statuses for which it was calculating output and output signals values.
See `testOutput` for how it looks.
