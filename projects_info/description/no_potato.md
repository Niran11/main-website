Kind of multiplayer medieval knight roleplay.
Player has people to manage. To fight in battles he uses his army.
To create products he needs craftspeople.
To sell products to other players he needs to send merchants to sell items.
\
\
Players can also create countries and then conquer cities.
To conquer city they have to fight country which already owns that city.
Fighting depletes player's resources like army morale, weapons and armor,
which decreases player's damage and eventually army amount.
After battle player can receive equipment which improves his statistics.
Said equipment can be upgraded and downgraded by players as they need and can afford.
\
\
Each city has council consisting of real players.
That council is elected in elections each month.
Council can create laws that change how city functions.
\
\
Players can also mine ores in mines and eventually use extracted and processed materials
to craft buffs improving player's statistics for limited time.
\
\
To refill their loss in people players may try to recruit new people,
but who knows if anyone will want to go with them.
\
\
Pretty similar game to what `etmp` was, but in more medieval version.
Other major change is that this project uses elixir + phoenix live view, which allows to immediate
updates thanks to phoenix's `PubSub` and websocket connection.
