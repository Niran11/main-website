Discord bot with functionalities such as:
- Adding quotes, displaying them, etc.
- Reposting messages by using embed with original poster's name and his message text
- Organizing giveaways, with ability to limit it to specified roles
- Todo lists - one server can have multiple lists (as long as they have unique names)
- Autothreads - creating threads to new messages in some channel
- Member join/leave messages - configure channel, text, image to be shown when member joins/leaves server
- Member change logs - timeout/kick/ban/unban actions logging
- Giving reactions to each new message in given channel
- Embed creation & giving roles based on reactions

\
It stores all files in discord channel, which has some limitations due to files urls expiring.
Because of that some messages will stop having a image after some time (eg. quotes),
but they can be requested again which should show image properly, as discord refreshes images urls.
\
\
Storage channel can be configured via command. In such case bot will store files in provided channel.
If channel is not given then it will use its global channel.
\
\
It also supports Polish and English language which can be set using command, but commands text will remain in English.
