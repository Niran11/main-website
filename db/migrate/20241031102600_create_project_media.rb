class CreateProjectMedia < ActiveRecord::Migration[7.2]
  def change
    create_table :project_media do |t|
      t.references :project, null: false, index: true, foreign_key: true
      t.string :filename, null: false

      t.timestamps
    end

    add_index :project_media, [:project_id, :filename], unique: true
  end
end
