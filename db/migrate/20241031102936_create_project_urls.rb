class CreateProjectUrls < ActiveRecord::Migration[7.2]
  def change
    create_table :project_urls do |t|
      t.references :project, null: false, index: true, foreign_key: true
      t.string :url, null: false
      t.string :label, null: false

      t.timestamps
    end

    add_index :project_urls, [:project_id, :url], unique: true
  end
end
