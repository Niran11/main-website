class CreateProjects < ActiveRecord::Migration[7.2]
  def change
    create_table :projects do |t|
      t.string :name, null: false
      t.string :code, null: false
      t.string :summary, null: false
      t.string :description, null: false
      t.integer :weight, null: false

      t.timestamps
    end

    add_index :projects, :name, unique: true
    add_index :projects, :code, unique: true
  end
end
