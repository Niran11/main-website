require "toml-rb"

desc "refreshes informations about projects in db"
task reload_projects: [:environment] do
  projects_config_path = Rails.root.join("projects_info", "config")
  projects_description_path = Rails.root.join("projects_info", "description")
  Dir.glob(projects_config_path.to_s + "/*.toml") do |path|
    puts("Loading: \"#{path}\"")
    config = TomlRB.load_file(path, symbolize_keys: true)
    description = File.read projects_description_path.join(config[:code] + ".md")
    project = Project.find_by(code: config[:code])
    p_config = config.slice(:name, :code, :summary, :weight)
    p_config[:description] = description
    save_result = if project
        project.update(p_config)
      else
        project = Project.new(p_config)
        project.save
      end
    if !save_result
      puts("ERROR: failed to update project #{config[:code]}: #{project.errors.full_messages}")
      return
    end
    project.update_tags config[:tags]
    project.update_urls config[:urls]
    project.update_media config[:media]
  end
end
