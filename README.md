# What it is
Page containing my projects made over the years.
It has description of each of them and images showing how project is/was looking.

# Running
Development:
```sh
bin/setup
bin/rails reload_projects
bin/dev
```

Deployment:
```sh
export RAILS_ENV=production
EDITOR="nano" bin/rails credentials:edit
bin/setup
bin/rails reload_projects
bundle exec bootsnap precompile app/ lib/
bin/rails assets:precompile
bin/rails s
```
